## [1.2.2](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/compare/v1.2.1...v1.2.2) (2021-10-06)


### Bug Fixes

* ensure te_files are saved idempotently ([1e41b7b](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/commit/1e41b7b))

## [1.2.1](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/compare/v1.2.0...v1.2.1) (2021-09-23)


### Bug Fixes

* Flush handlers ([699da9c](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/commit/699da9c))

# [1.2.0](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/compare/v1.1.0...v1.2.0) (2021-09-22)


### Features

* remove ansible_role_packages ([f92c3e1](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/commit/f92c3e1))

# [1.1.0](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/compare/v1.0.0...v1.1.0) (2021-03-17)


### Features

* updates to support ansible 2.10.6 + pipeline fixes ([02f7436](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/commit/02f7436))

# 1.0.0 (2019-11-07)


### Bug Fixes

* Update variable for changes in bare variable ([4e59451](https://gitlab.com/dreamer-labs/maniac/ansible_role_security/commit/4e59451))
