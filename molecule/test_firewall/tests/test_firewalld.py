import os
import testinfra.utils.ansible_runner
import urllib3

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_firewalld_enabled(host):
    assert host.service('firewalld').is_running
    assert host.service('firewalld').is_enabled

def test_firewalld_rules(host):
    rule = '-A IN_public_allow -p tcp -m tcp --dport 80 -m conntrack --ctstate NEW,UNTRACKED -j ACCEPT'

    assert rule in host.iptables.rules(chain='IN_public_allow')

def test_firewalld_port_open(host):
    http = urllib3.PoolManager()
    external_ip = host.interface('eth0').addresses
    try:
        response = http.request('GET', f'http://{external_ip[0]}', timeout=3.0)
        assert type(response.status) == int
    except urllib3.exceptions.MaxRetryError:
        assert False, f"IP {external_ip[0]} is not accessible."
