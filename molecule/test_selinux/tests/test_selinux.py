import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_selinux_enabled(host):
    selinux_enabled = host.run('sudo getenforce')

    assert selinux_enabled.stdout.rstrip() == 'Enforcing'

def test_selinux_module_added(host):
    module_test = host.run('sudo semanage module -e zabbix_server_add')

    assert module_test.rc == 0

def test_selinux_boolean_added(host):
    boolean_test = host.run('sudo getsebool -a | grep httpd_can_network_connect | grep on')

    assert boolean_test.rc == 0

def test_selinux_boolean_removed(host):
    boolean_test = host.run('sudo getsebool -a | grep xend_run_qemu | grep off')

    assert boolean_test.rc == 0

def test_selinux_port_added(host):
    port_test = host.run('sudo semanage port -lC | grep http_port_t | grep 8880')

    assert port_test.rc == 0
